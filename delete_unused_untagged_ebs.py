import boto3
from pprint import pprint
session=boto3.Session(profile_name="aws_root")
ec2_re=session.resource(service_name='ec2',region_name='us-east-1')
ec2_client=session.client(service_name='ec2',region_name='us-east-1')

# # List of all EBS Volume
# # pprint( dir(ec2_re))
# for each_volume in ec2_re.volumes.all():
#     print (each_volume.id, each_volume.state)
    
# #####USing Fileter###########
# febsu={'Name':'status','Values':['available']}
# for each_volume in ec2_re.volumes.filter(Filters=[febsu]):
#     print (each_volume.id, each_volume.state, each_volume.tags)

all_required_volume_ids=[]


# read for delete which is non Tag volume

for each_volume in ec2_re.volumes.all():
    if each_volume.state=="available" and each_volume.tags==None:
        print (each_volume.id, each_volume.state, each_volume.tags)
        all_required_volume_ids.append(each_volume.id)

#delete volume:
for each_volume in all_required_volume_ids:
     volume_object=ec2_re.Volume(each_volume)
     print ("Deleteing volume of id ",each_volume)
     volume_object.delete()
waiter= ec2_client.get_waiter('volume_deleted')
try:
    waiter.wait(VolumeIds= all_required_volume_ids )
    print("Successfully deleted all your volumes which is untagged EBS Volumes")
except Exception as e:
    print(e)
     
     
