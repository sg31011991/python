import boto3
import pprint
session=boto3.Session(profile_name="aws_root",region_name="us-east-1")
client_ec2 = boto3.client('ec2')
resource_ec2 = boto3.resource('ec2')

# ### using resource object
for each_instance in resource_ec2.instances.all():
    print(each_instance)
    print(each_instance.id, each_instance.state)
    print(each_instance.id, each_instance.state['Name'])

for each_instance in client_ec2.describe_instances()['Reservations']:
    pprint.pprint(each_instance)
    for each in each_instance['Instances']:
        print(each['InstanceId'],each['State']['Name'])
