import os,sys
try:
    import boto3
    print("imported boto3 successfully")
except Exception as e:
    print(e)
    sys.exit(1)

source_region='us-east-1'
dest_region='us-east-2'
session=boto3.session.Session(profile_name="aws_root")
sts_client=session.client(service_name='sts',region_name='us-east-1')
account_id=sts_client.get_caller_identity().get('Account')
ec2_source_client=session.client(service_name="ec2",region_name=source_region)
bkp_snap=[]
f_bkp={'Name':'tag:test','Values':['demo-ebs-2']}
for each_snap in ec2_source_client.describe_snapshots(OwnerIds=[account_id],Filters=[f_bkp]).get('Snapshots'):
    ####### Getting Snapshots ALL information ############
    print("### Getting Snapshots ALL information ####")
    print(each_snap)
    print("#######..Getting only SNAPSHOT ID..########")
    print(each_snap.get('SnapshotId'))
    bkp_snap.append(each_snap.get('SnapshotId'))
ec2_destination_client=session.client(service_name="ec2",region_name=dest_region)
for each_source_snapid in bkp_snap:
    print("Taking backup for id of {} into a {}".format(each_source_snapid,dest_region))
    ec2_destination_client.copy_snapshot(
    Description='for DR',
    SourceRegion=source_region,
    SourceSnapshotId=each_source_snapid
    )
print("EBS Snapshot copy destination region is complated")
print("Modifing tags for the snapshorts")
for each_source_snapid in bkp_snap:
    print("Deleting old Tags for {}".format(each_source_snapid))
    ec2_source_client.delete_tags(
        Resources=[each_source_snapid],
        Tags=[
        {
            'Key': 'test',
            'Value': 'demo-ebs-2'
        },
      ]
    )
    print("Creating New Tags for {}".format(each_source_snapid))
    ec2_source_client.create_tags(
        Resources=[each_source_snapid],
        Tags=[
        {
            'Key': 'backup',
            'Value': 'yes'
        },
      ]
    )
    ##Demo#######
    
