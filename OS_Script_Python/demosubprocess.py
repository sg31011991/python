import subprocess
cmd="dir"
#cmd="ls -lart"
sp=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,universal_newlines=True)
rc=sp.wait()
out,err=sp.communicate()
print(f'The return code: {rc}')
print(f'The output: \n {out}')
print(f'The error: \n {err}')
#######when output as a list ########
cmd="ls -lrt".split()  ### need to mention shell=False
sp=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,universal_newlines=True)
rc=sp.wait()
out,err=sp.communicate()
print(f'The return code: {rc}')
print(f'The output: \n {out.splitlines()}')
print(f'The error: \n {err.splitlines()}')
