########## for one region##########
import boto3
from pprint import pprint
session=boto3.Session(profile_name="aws_root")
ec2_client=session.client(service_name="ec2",region_name="us-east-1")
# pprint(ec2_client.describe_volumes()['Volumes'])
list_of_voi_ids=[]

# ##########With out tag filter###############
# for each_vol in ec2_client.describe_volumes()['Volumes']:
#     # pprint(each_vol['VolumeId'])
#     list_of_voi_ids.append(each_vol['VolumeId'])
# print ("The list of volids are: ",list_of_voi_ids)

######################################################
f_prod_bkp={'Name':'tag:test','Values':['demo-ebs-2']}
# for each_vol in ec2_client.describe_volumes(Filters=[f_prod_bkp])['Volumes']:
#     # pprint(each_vol['VolumeId'])
#     list_of_voi_ids.append(each_vol['VolumeId'])

#########USing Paginator#####################
paginator = ec2_client.get_paginator('describe_volumes')
for each_page in paginator.paginate(Filters=[f_prod_bkp]):
    for each_vol in each_page['Volumes']:
       list_of_voi_ids.append(each_vol['VolumeId']) 
print ("The list of volume_ids are: ",list_of_voi_ids)
snapids=[]
for each_volid in list_of_voi_ids:
    print ("Taking snap of {}".format(each_volid))
    res=ec2_client.create_snapshot(
             Description='Taking snap with lambda & Cloud watch',
             VolumeId=each_volid,
             TagSpecifications=[
                    {
                        'ResourceType':'snapshot',
                        'Tags': [
                            {
                                'Key': 'Delete-on',
                                'Value': '90'
                            }
                        ]
                    }
             ]
           )
    snapids.append(res.get('SnapshotId'))
print ("The snap ids are: ",snapids)
waiter = ec2_client.get_waiter('snapshot_completed')
waiter.wait(SnapshotIds=snapids)

print("Successfully completed snap for the volumesof {}",list_of_voi_ids)



