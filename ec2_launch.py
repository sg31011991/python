#aws ec2 create-key-pair --key-name KeyPair1 --query 'KeyMaterial' --output text > KeyPair1.pem 
import boto3
session=boto3.Session(profile_name="aws_root")
ec2_client=session.client(service_name="ec2",region_name="us-east-1")
def create_instance():
    response = ec2_client.run_instances(
    ImageId='ami-0dbc3d7bc646e8516',
    InstanceType='t2.micro',
    MaxCount=1,
    MinCount=1,
    KeyName="KeyPair1",
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'MyDemoInstance'
                },
                ]
            },
        ]     
    )
    
    instance_id = response["Instances"][0]["InstanceId"]
    print(instance_id)
    waiter = ec2_client.get_waiter('instance_status_ok')
    waiter.wait(InstanceIds=[instance_id])
    print("Instance has been created")
create_instance()

#get list of all running instances
def get_running_instances():
    reservations = ec2_client.describe_instances(Filters=[
        {
            "Name": "instance-state-name",
            "Values": ["running"],
        }
    ]).get("Reservations")

    for reservation in reservations:
        for instance in reservation["Instances"]:
            instance_id = instance["InstanceId"]
            instance_type = instance["InstanceType"]
            public_ip = instance["PublicIpAddress"]
            private_ip = instance["PrivateIpAddress"]
            print(f"{instance_id}, {instance_type}, {public_ip}, {private_ip}")           
get_running_instances()


