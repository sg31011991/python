import os
import datetime

file_age=2
today_date=datetime.datetime.now()

for dir,dirpath,filename in os.walk("C:\\Temp\\demo"):
    for file in filename:
        complete_path=os.path.join(dir,file)
        file_creation_time=datetime.datetime.fromtimestamp(os.path.getctime(complete_path))
        time_diff=(today_date-file_creation_time).days
        print(complete_path,file_creation_time,time_diff)
        if time_diff> file_age:
            os.remove(complete_path)
            print(complete_path, time_diff)
